/* eslint-disable no-unused-vars */

// FIXME: Extensions may be loaded when they are needed.

import { get_json } from '../utils/network';
import MathJax from './mathjax';
import registerPrism from './prism';

async function get_packages() {
  let config = await get_json('config/manju-show-js.json');
  return config['imports'];
}

function get_url(config, key, defaultUrl) {
  if (config && config[key]) {
    return config[key];
  } else {
    return defaultUrl;
  }
}

export default async function registerExtensions() {
  let config = await get_packages();

  // Mathjax
  MathJax.register(get_url(config, 'mathjax', 'https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js'));

  // Prism.js
  registerPrism(get_url(config, 'prism-core', 'https://cdn.jsdelivr.net/npm/prismjs@1.20.0/components/prism-core.min.js'))
    .then((script) => { console.log('load prism-core.min.js'); });
  registerPrism(get_url(config, 'prism-autoloader', 'https://cdn.jsdelivr.net/npm/prismjs@1.20.0/plugins/autoloader/prism-autoloader.min.js'))
    .then((script) => { console.log('load prism-autoloader.min.js'); });
}

export { triggerMathJax } from './mathjax';
